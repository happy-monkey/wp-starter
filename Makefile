
build:
	docker-compose build

up:
	docker-compose up --force-recreate

test: build up